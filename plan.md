# 100 Days Of Code - Plan

I plan learning Ruby, React.js and Vue.js by developing a blog site and improve my [portfolio site](https://bolabuari.com/).

#### Things to Learn - With Side Projects
 - [x] Ruby
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] Ruby on Rails
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] Testing Ruby
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] Testing RoR
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] HTML/CSS
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] SVG
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] JavaScript
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] React.js 
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3
 - [x] Vue.js
	 - [x] Project 1
	 - [x] Project 2
	 - [x] Project 3

#### *This is will be updated as plan changes*

## Contents
* [Rules](rules.md)
* [Log - click here to see my progress](log.md)
* [Plan - click here to see my plan](plan.md)
* [FAQ](FAQ.md)
* [Resources](resources.md)
* [Web](https://bolabuari.com/) - [Twitter](https://twitter.com/bolah2009) - [GitHub](https://github.com/bolah2009/) - [GitLab](https://gitlab.com/bolah2009/) - [LinkedIn](https://www.linkedin.com/in/bolah2009/)


